package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText etNumber1, etNumber2;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        if(isEmpty(etNumber1) == false && isEmpty(etNumber2) == false) {
            double num1, num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 + num2));
        } else {
            result.setText(R.string.requirement);
        }
    }  //addNums()

    public void subtractNums(View v) {
        if(isEmpty(etNumber1) == false && isEmpty(etNumber2) == false) {
            double num1, num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 - num2));
        } else {
            result.setText(R.string.requirement);
        }
    } //subtractNums()

    public void multiplyNums(View v) {
        if(isEmpty(etNumber1) == false && isEmpty(etNumber2) == false) {
            double num1, num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 * num2));
        } else {
            result.setText(R.string.requirement);
        }
    } //multipleNums()

    public void divideNums(View v) {
        if(isEmpty(etNumber1) == false && isEmpty(etNumber2) == false) {
            double num1, num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            if (num2 == 0) {
                result.setText(R.string.divideZero);
            } else {
                result.setText(Double.toString(num1 / num2));
            }
        } else {
            result.setText(R.string.requirement);
        }
    } //divideNums()

    public void clearNums(View v) {
        etNumber1.getText().clear();
        etNumber2.getText().clear();
        result.setText(R.string.answer);
    } //clearNums()

    public boolean isEmpty(EditText etText) {
        if(etText.getText().toString().trim().length() > 0) {
            return false;
        }
        return true;
    } //isEmpty()
}